const Course = require("../models/Course");
const auth = require("../auth");

// reqBody will be the information getting addcourse
module.exports.addCourse = (reqBody) => {


	let newCourse = new Course ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	// newCourse will be saved in the data 
	return newCourse.save().then((course, error) => {

		if (error) {

			return false
		} else {
			return course
		}
	})
}