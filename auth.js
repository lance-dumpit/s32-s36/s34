const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token Creation 
// createAccessToken - when logging in
module.exports.createAccessToken = (user) => {

	// When the user login, the token will be created with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// generate a JSON we token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided.

	return jwt.sign(data, secret, {});

} 

module.exports.verify = (req,res, next) => {

	// if authorized
	// headres is in postman

	let token = req.headers.authorization;

	// check / return the data type (typeof) of the token
	// token has laman
	if(typeof token !== "undefined") {
		console.log(`${token} -- Before slice`);

		// slice to get the token and remove the bearer (bearer 12fbbwbq2i3)
		// if not undefined will slice the token
		// 7th in the string which is the bearer

		//Syntax: string.slice(start, end)
		// token.lenght will be the remaining
		token = token.slice(7, token.length);

		console.log(`${token} -- after slice`)

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return res.send({auth: "failed"});

			} else {
				// the ano will proceed
				next();
			}

		})
	} else {
		// if failed/undefined
		return res.send({auth: "failed"});
	}

}

// 
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return null
			} else {

				// to only get the payload
				// if no .payload will get everything from the header
				return jwt.decode(token, {complete: true}).payload;
			}
		})

		} else {

			return null
		}
}

