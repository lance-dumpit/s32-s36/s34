const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");


//Route for creating a course
router.post("/",auth.verify, (req,res) => {


	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	if (userData.isAdmin == true) {
		courseController.addCourse(req.body).then(resultFromController => res.send(
		resultFromController))
	} else {
		return false
	}
}) 

module.exports = router;
